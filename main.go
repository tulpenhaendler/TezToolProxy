package main

import (
	"encoding/json"
	"fmt"
	"github.com/gin-gonic/gin"
	"io/ioutil"
	"net/http"
	"os"
)

func main() {
	fmt.Println("Starting TezProxy")

	token := os.Getenv("TOKEN")
	targetstr := os.Getenv("Targets")
	targets := map[string]string{}
	json.Unmarshal([]byte(targetstr), &targets)

	r := gin.Default()
	cl := http.Client{}

	r.Any("/:target/*url", func(c *gin.Context) {
		if c.GetHeader("TOKEN") != token {
			c.Abort()
			return
		}
		if _, ok := targets[c.Param("target")]; !ok {
			c.Abort()
			return
		}
		url := targets[c.Param("target")] + c.Param("url")
		req, err := http.NewRequest(c.Request.Method, url, c.Request.Body)
		resp, err := cl.Do(req)
		if err != nil {
			c.Abort()
			return
		}
		body, _ := ioutil.ReadAll(resp.Body)
		c.String(200, string(body))
		return
	})

	fmt.Println(r.Run(":9999"))

}

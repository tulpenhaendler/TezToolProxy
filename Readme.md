# TezToolProxy

A simple Proxy Server so TezTool can access things like the tezos-node rpc without exposing the
port publicly and without auth on the Host System
